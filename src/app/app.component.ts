import {Component, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {User} from './model/user';
import {UserStatus} from './model/userStatus';
import {StatisticResp} from './model/statisticResp';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('fileInput', {static: false}) fileInput;
  url = 'http://localhost:8080';
  user: User;
  userStatus: UserStatus;
  statistic: StatisticResp;
  searchUser: User;
  isRegister = false;
  isFind = false;
  idForSearch: number;
  users: User[];
  isStatusChanged = false;

  constructor(private http: HttpClient) {
    this.user = new User();
    this.userStatus = new UserStatus();
    this.statistic = new StatisticResp();
    this.statistic.status = 'no';
  }

  onSubmit() {
    console.log(this.fileInput.nativeElement.files[0]);
    const formData = new FormData();
    formData.append('file', this.fileInput.nativeElement.files[0]);
    this.http.post(this.url + '/upload', formData).subscribe((result: any) => {
      alert('аватар загружен');
      console.log(result);
      this.user.avatar = result.message;
    }, (error) => {
      alert('ошибка загрузки аватара');
      console.log(error);
    });
  }

  onSubmitUser() {
    console.log(this.user);
    this.http.post(this.url + '/user', this.user).subscribe((result: any) => {
      alert('пользователь с id = ' + result + ' зарегестрирован');
      this.isRegister = false;
    }, (error) => {
      alert('ошибка регистрации');
      console.log(error);
    });
  }

  getUserById() {
    if (this.idForSearch > 0) {
      this.http.get(this.url + '/user/' + this.idForSearch).subscribe((user: User) => {
        this.searchUser = user;
        if (!(this.searchUser.userStatus && this.searchUser.userStatus.newStatus)) {
          this.userStatus.newStatus = 'no';
        } else {
          this.userStatus = this.searchUser.userStatus;
        }
        console.log(user);
      }, (error) => {
        this.searchUser = null;
        alert(error.error);
      });
    } else {
      alert('Неверный ввод id!');
    }
  }

  register() {
    this.isRegister = true;
    this.isFind = false;
  }

  find() {
    this.isRegister = false;
    this.isFind = true;
  }

  getUserStatus(id: number) {
    if (this.isStatusChanged) {
      const header = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
      let body = new HttpParams();
      body = body.set('userId', id.toString());
      if (!this.userStatus || this.userStatus.newStatus === 'no') {
        body = body.set('status', null);
      } else {
        body = body.set('status', this.userStatus.newStatus);
      }
      this.http.post(this.url + '/user/status/update', body, {
        headers: header
      }).subscribe((result: any) => {
        alert('Статус пользователя c id = ' + result.id + ' изменен c ' + result.oldStatus + ' на ' + result.newStatus + '.');
      }, (error) => {
        console.log(error);
      });
      this.isStatusChanged = false;
    } else {
      alert('статус не был изменен');
    }
  }

  statusChange() {
    this.isStatusChanged = true;
  }

  getStatisticsByStatus() {
    console.log(this.statistic);
    const header = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    let body = new HttpParams();
    body = body.set('status', this.statistic.status);
    if (this.statistic.date) {
      body = body.set('date', this.statistic.date.toString());
    } else {
      body = body.set('date', null);
    }
    this.http.post(this.url + '/statistics', body, {
      headers: header
    }).subscribe((result: any) => {
      console.log(result);
      this.users = result;
    }, (error) => {
      console.log(error);
    });
  }
}
