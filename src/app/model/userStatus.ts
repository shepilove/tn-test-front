export class UserStatus {
  id: number;
  changeStatusDateTime: Date;
  oldStatus: string;
  newStatus: string;
  userId: number;
}
