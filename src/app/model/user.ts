import {UserStatus} from './userStatus';

export class User {
  id: number;
  firstName: string;
  surName: string;
  nickName: string;
  email: string;
  avatar: string;
  userStatus: UserStatus;
}
